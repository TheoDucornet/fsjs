const express = require("express")

module.exports.home = (req,res)=>{
    res.render('pages/home')
}

const autorization = require('./autorization')
const {authorize} = require("./autorization");
module.exports.login = (req,res)=>{
    let userId = 666
    autorization.createToken(res,userId);
    res.redirect('/admin')
}

module.exports.admin = (req,res)=>{
    let userId = req.userId
    res.render('pages/admin',{userId})
}

module.exports.logout = (req,res)=>{
    autorization.clearToken(res)
    res.redirect('/')
}