const express = require('express')
const router = express.Router()
const autorization = require('../controllers/autorization')

const mainController = require('../controllers/mainController')
router.get('/',mainController.home)
router.get('/admin',autorization.authorize,mainController.admin)
router.get('/login',mainController.login)
router.get('/logout',mainController.logout)

module.exports = router