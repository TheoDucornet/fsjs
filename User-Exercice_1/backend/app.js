const express= require('express')
const app = express()

//--Accès à public
const path = require('path')
app.use(express.static(path.join(__dirname,'public')))

//--Moteur de rendu
app.set('view engine','ejs')
app.set('views',path.join(__dirname,'views'))

//--Gestion des layouts
const expressLayouts = require('express-ejs-layouts')
app.use(expressLayouts)
app.set('layout','../views/layouts/layout')

//--Activation des cookies
const cookieParser = require('cookie-parser') ;
app.use(cookieParser()) ;

//--Gestion du routage
const mainRouter = require('./routes/mainRouter')
app.use('/',mainRouter)

module.exports = app