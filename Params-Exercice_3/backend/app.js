const express = require('express')
const app = express()

//Middleware qui retourne les fichiers du dossier public
//A appeler avant toute modification de
const path = require('path')
app.use(express.static(path.join(__dirname,'public')))

//Définition view engine:
// moteur de rendu:
app.set('view engine', 'ejs')
// dossier contenant les vues:
app.set('views',path.join(__dirname,'views'))

//Utilisation de express-ejs-layout:
const expressLayouts = require('express-ejs-layouts')
//ajout du middleware
app.use(expressLayouts)
//définition layout par défaut
app.set('layout','../views/layouts/layout')

//--Activation du POST
app.use(express.json())
app.use(express.urlencoded({extended:false}))

//--Utilisation du GET
app.get("/",(req,res)=>{
    let query = req.query
    res.render('pages/show_get_params',query)
})

app.get('/user/:id',(req,res) =>{
    res.render('pages/user',{id_value : req.params.id})
})

app.get("/form",(req,res)=>{
    res.render('pages/form')
})

//--Utilisation de route POST
app.post('/form',(req,res)=>{
    let data=req.body
    //Faut pas nommer la variable body sinon conflit avec EJS
    res.render('pages/show_post_data',{data})
})

module.exports = app