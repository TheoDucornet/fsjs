const port = process.env.PORT || 3001

const http = require('http')

const server = http.createServer((req, res) =>{
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html;charset=utf-8');
    res.end("Le serveur Node.js dit <b>Yo</b>")
})

server.listen(port,()=>{
    console.log(`Le server écoute sur http://127.0.01:${port}/`);
})