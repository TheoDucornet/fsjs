const express = require('express')
const app = express()

//Middleware qui retourne les fichiers du dossier public
//A appeler avant toute modification de
const path = require('path')
app.use(express.static(path.join(__dirname,'public')))

//Définition view engine:
// moteur de rendu:
app.set('view engine', 'ejs')
// dossier contenant les vues:
app.set('views',path.join(__dirname,'views'))

//Utilisation de express-ejs-layout:
const expressLayouts = require('express-ejs-layouts')
//ajout du middleware
app.use(expressLayouts)
//définition layout par défaut
app.set('layout','../views/layouts/layout')

app.use((req,res) =>{
    res.render('pages/home',{ user : {nickname:'Theo',sex:'homme'}})
})

module.exports = app