const express= require('express')
const app = express()

//--Accès à public
const path = require('path')
app.use(express.static(path.join(__dirname,'public')))

//--Moteur de rendu
app.set('view engine','ejs')
app.set('views',path.join(__dirname,'views'))

//--Gestion des layouts
const expressLayouts = require('express-ejs-layouts')
app.use(expressLayouts)
app.set('layout','../views/layouts/layout')

//--Gestion du routage
const homeRouter = require('./routes/homeRouter')
app.use('/',homeRouter)

const randomRouter = require('./routes/randomRouter')
app.use('/random',randomRouter)

//--Utilisation de route GET
const jokeRouter = require('./routes/jokeRouter')
app.use('/joke',jokeRouter)

module.exports = app