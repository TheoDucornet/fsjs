const express = require("express")

module.exports.home = (req,res)=>{
    //--Lecture du fichier Jokes.json:
    const fs = require('fs')
    const path = require('path')
    fs.readFile(path.join(__dirname,'../public/data/jokes.json'),'utf-8',(err,data)=>{
        if(err) {
            console.log('Erreur :')
            console.log(err)
            return
        }
        //Mise en forme des données
        var result = JSON.parse(data)
        //Creation de la page:
        res.render('pages/home',{jokes:result})
    })
}