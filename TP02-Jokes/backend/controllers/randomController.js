const express = require("express")
const fs = require("fs");
const path = require("path");

module.exports.home = (req,res)=>{
    //--Lecture du fichier Jokes.json:
    const fs = require('fs')
    const path = require('path')
    fs.readFile(path.join(__dirname,'../public/data/jokes.json'),'utf-8',(err,data)=>{
        if(err) {
            console.log('Erreur :')
            console.log(err)
            return
        }
        //Mise en forme des données
        results = JSON.parse(data)
        result = results[Math.floor(Math.random() * results.length)]

        //Creation de la page:
        res.render('pages/random',{joke:result})
    })
}