const express = require("express")

module.exports.home = (req,res)=>{
    //--Lecture du fichier Jokes.json:
    const fs = require('fs')
    const path = require('path')
    fs.readFile(path.join(__dirname,'../public/data/jokes.json'),'utf-8',(err,data)=>{
        if(err) {
            console.log('Erreur :')
            console.log(err)
            return
        }
        //Mise en forme des données
        var results = JSON.parse(data)
        //Recuperation de la blague souhaitée
        var result = undefined
        for (index in results){
            //console.log(results[index],"==",req.params.id)
            if (results[index]['id']==req.params.id){
                result = results[index]
            }
        }
        //console.log(result)
        //Creation de la page:
        res.render('pages/joke',{joke:result})
    })
}