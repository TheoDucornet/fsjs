const express = require('express')
const app = express()

const path = require('path')


//Middleware qui retourne les fichiers du dossier public
//A appeler avant toute modification de res
app.use(express.static(path.join(__dirname,'public')))


//Middleware d'affichage console
app.use((req,res,next) =>{
    const now = new Date().toString()
    console.log(`${now} : une requête ${req.method} est arrivée !`)
    next() //Transmet l'appel au prochain middleware
})

module.exports = app