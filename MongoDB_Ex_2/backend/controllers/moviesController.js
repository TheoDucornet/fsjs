const movie = require('../models/movie')

module.exports.home = (req,res)=>{
     movie.find()
         .sort({title:'asc'})
         .then(movies =>{
             res.render('pages/movies',{movies})
         })
         .catch(error => res.status(400).send(error))
}

module.exports.create = (req,res)=>{
    const movieData = {
        title:'House'
    }
    movie.create(movieData)
        .then(res.redirect('/movies'))
        .catch(error => res.status(400).send(error))
}

module.exports.read = (req,res)=>{
    movie.findOne({title:'House'})
        .then((movie)=>res.render('pages/read',{movie}))
        .catch(error => res.status(400).send(error))
}

module.exports.update = (req,res)=>{
    movie.updateOne({title:'House'},{synopsis:'Une super histoire qui fait peur... et rire !'})
        .then(res.redirect('/movies'))
        .catch(error => res.status(400).send(error))
}

module.exports.delete = (req,res)=>{
    movie.deleteOne({title:'House'})
        .then(res.redirect('/movies'))
        .catch(error => res.status(400).send(error))
}