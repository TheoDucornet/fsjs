const express = require('express')
const app = express()


//1er middleware
app.use((req,res,next) =>{
    const now = new Date().toString()
    console.log(`${now} : une requête ${req.method} est arrivée !`)
    next() //Transmet l'appel au prochain middleware
})

//2ème middleware
app.use((req,res,next)=> {
    res.statusCode = 200
    res.setHeader('Content-Type','text/html;charset=utf-8')
   next()
})

//3ème middleware
app.use((req,res) =>{
    res.end(('Le serveur Express dit <b>test</b>'))
})

module.exports = app