const express = require('express')
const router = express.Router()

router.get('/',(req,res)=>{
    res.render('pages/enseignement')
})

router.get('/javascript',(req,res)=>{
    res.render('pages/javascript')
})

router.get('/php',(req,res)=>{
    res.render('pages/php')
})

router.get('/node',(req,res)=>{
    res.render('pages/node')
})

router.get('/node/express',(req,res)=>{
    res.render('pages/express')
})

module.exports = router