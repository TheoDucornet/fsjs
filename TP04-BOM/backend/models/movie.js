const mongoose = require('mongoose')

const movieSchema = mongoose.Schema({
    //Il y a aussi un champ id automatiquement crée
    title:{ type:String, require: true},
    synopsis:{type: String, default: "No synopsys available yet"},
    image:{type: String}
})

module.exports = mongoose.model('Movie',movieSchema)

//Le nom de collection 'movies' est automatiquement déduit du nom du schéma 'Movie'.
//Sinon il faut le préciser: module.exports = mongoose.model('Movie',movieSchema,'movies')