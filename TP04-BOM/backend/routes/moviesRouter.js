const express = require('express')
const router = express.Router()

const moviesController = require('../controllers/moviesController')
router.get('/',moviesController.home)
router.get('/:id',moviesController.read)

module.exports = router