const express = require('express')
const app = express()

const path = require('path')


app.use(express.static(path.join(__dirname,'public')))

//Middleware pour notifier d'une requête
app.use((req,res,next) =>{
    const now = new Date().toString()
    console.log(`${now} : une requête ${req.method} est arrivée !`)
    next() //Transmet l'appel au prochain middleware
})

module.exports = app