const express = require('express')
const app = express()
app.use((req,res)=> {
    res.statusCode = 200
    res.setHeader('Content-Type','text/html;charset=utf-8')
    res.end(('Le serveur Express dit <b>test</b>'))
})

module.exports = app