const express= require('express')
const app = express()

//--Accès à public
const path = require('path')
app.use(express.static(path.join(__dirname,'public')))

//--Moteur de rendu
app.set('view engine','ejs')
app.set('views',path.join(__dirname,'views'))

//--Gestion des layouts
const expressLayouts = require('express-ejs-layouts')
app.use(expressLayouts)
app.set('layout','../views/layouts/layout')

//--Activation du POST
app.use(express.json())
app.use(express.urlencoded({extended:false}))

//--Gestion MongoDB
const mongoose = require('mongoose')

//Connexion à un serveur MongoDB local sans gestion des accès
const host = '127.0.0.1'
const port = '27017'
const db_name = 'movies_db'
const mongoDB = `mongodb://${host}:${port}/${db_name}?retryWrites=true&w=majority`

mongoose.connect(mongoDB,{useUnifiedTopology:true})
    .then(()=>console.log('MongoDB OK !'))
    .catch(()=>console.log('MongoDB ERREUR'))

const db = mongoose.connection

db.on('error',console.error.bind(console,'MongoDB connection error:'))


//--Gestion du routage
const homeRouter = require('./routes/homeRouter')
app.use('/',homeRouter)

const moviesRouter = require('./routes/moviesRouter')
app.use('/movies',moviesRouter)

module.exports = app