const movie = require('../models/movie')

module.exports.home = (req,res)=>{
     movie.find()
         .sort({title:'asc'})
         .then(movies =>{
             res.render('pages/movies',{movies})
         })
         .catch(error => res.status(400).send(error))
}