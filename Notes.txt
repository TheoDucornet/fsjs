Pour utiliser PC Fac:
PATH=/usr/local/nvm/versions/node/v.15.12.0/bin:$PATH
(ds chaque nouveau terminal)

Pour Webstorm:
/usr/local/Webstorm.../bin/webstorm.sh

Pour commencer de zero:
npm init

--Partie MongoDB
MongoDB en local sur machina FAC:
"Dans les salles de TP, vous trouverez les dossiers d'installation de mongoDB (contenant mongod) et de mongosh dans le dossier /usr/local/"

=>Pour démarrer MongoDB :
cd "C:\Program Files\MongoDB\Server\5.0\bin"
mongod --dbpath C:\Users\ducor\Documents\mongodata
***PC Fac:
** cd /usr/local/mongodb-linux-x86_64-debian10-5.0.8/bin
** ./mongod --dbpath /etudiants/tducorne/Documents/mongodata/

=>Pour renommer la bdd:
Contrôle serveur via shell "mongosh"
use movies_db
***PC Fac:
**cd /usr/local/mongosh-1.4.1-linux-x64/bin/
**./mongosh
**use movies_db

=>Pour importer fichiers depuis Json:
cd C:\Program Files\MongoDB\Tools\100\bin
mongoimport --db=movies_db --collection=movies --file="C:\Users\ducor\Documents\movies.json" --jsonArray
***PC Fac:
** cd /usr/local/mongodb-database-tools-debian10-x86_64-100.5.2/bin
**./mongoimport --db=movies_db --collection=movies --jsonArray --file=/etudiants/tducorne/Documents/movies.json

=>Pr vider la BDD depuis mongosh:
use movies_db
db.movies.deleteMany({})

=>Pour voir le contenu d'une collection dans mongosh:
db.movies.find()
