const express= require('express')
const app = express()

//--Accès à public
const path = require('path')
app.use(express.static(path.join(__dirname,'public')))

//--Moteur de rendu
app.set('view engine','ejs')
app.set('views',path.join(__dirname,'views'))

//--Gestion des layouts
const expressLayouts = require('express-ejs-layouts')
app.use(expressLayouts)
app.set('layout','../views/layouts/layout')

//--Activation du POST
app.use(express.json())
app.use(express.urlencoded({extended:false}))

//--Gestion du routage
const homeRouter = require('./routes/homeRouter')
app.use('/',homeRouter)

const loginRouter = require('./routes/loginRouter')
app.use('/login',loginRouter)

module.exports = app