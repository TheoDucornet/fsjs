const express = require("express")

module.exports.home = (req,res)=>{
    res.render('pages/login',{data:undefined})
}

module.exports.homePOST = (req,res)=>{
    let data=req.body
    res.render('pages/login',{data})
}