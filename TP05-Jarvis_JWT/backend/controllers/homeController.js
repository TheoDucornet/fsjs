const express = require("express")
const autorization = require('./authorization')

module.exports.home = (req,res)=>{
    verifToken = autorization.authorize(req,res)
    if (verifToken === 'pasDeToken')
        res.render('pages/home',{data: undefined})
    else if (verifToken === 'TokenValide')
        //Ici, lire les données du token pour le nom d'utilisateur
        res.render('pages/home',{data: undefined})
    else
        res.render('pages/login')
}