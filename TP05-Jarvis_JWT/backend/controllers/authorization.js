const jwt = require('jsonwebtoken')

//Nom du cookie stocké:
const COOKIE_NAME = 'token'
//Secret=chaîne aléatoire pour cryptage:
const SECRET_KEY = 'testdusecretkey'
//Temps de validité du token:
const EXPIRATION = '30s'

module.exports.createToken = (res,userId)=>{
    const token = jwt.sign(
        //Payload sert à transporter infos (ici userId) :
        {userId},
        SECRET_KEY,
        {expiresIn: EXPIRATION}
    );
    console.log(token)
    //Enregistrement dans un cookie:
    res.cookie(COOKIE_NAME,token,{httpOnly: true})
}

module.exports.authorize = (req,res)=>{
    //Récupération du cookie
    const token = req.cookies[COOKIE_NAME];
    //Vérification de l'existence du cookie
    if (!token){
        return 'pasDeToken'
    }

    //Verification du contenu du token
    try  {
        const payload = jwt.verify(token,SECRET_KEY)
        req.userId = payload.userId
        return 'TokenValide'
    } catch {
        //Si pas valide:
        res.clearCookie(COOKIE_NAME)
        return 'TokenInvalide'
    }
}

module.exports.clearToken = (res)=>{
    res.clearCookie(COOKIE_NAME)
}