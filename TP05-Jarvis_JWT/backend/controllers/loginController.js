const express = require("express")
const {authorize} = require("./authorization");

module.exports.home = (req,res)=>{
    res.render('pages/login',{data:undefined})
}

module.exports.homePOST = (req,res)=>{
    //Où vérifier bons identifiants ? ici et pas créer le token ou ds page ? Probablement ici et pas besoin trimballer data pr infos page:
    //juste faire une page noir sur blanc avec ecrit bienvenue stark
    let data=req.body
    if(data.username == 'stark' && data.password == 'warmachinerox'){
        //creation token + renvoi route /
        authorize.createToken(res,666);
        res.redirect('/')
    }
    else
        //tester les diffts cas d'erreurs et ajouter ses infos dans l'appel à login
        //La redirection doit se faire en envoyant de quoi afficher les msgs d'erreur (passer controle de page login à ici
    res.render('pages/login',{data:data,error:''})
}