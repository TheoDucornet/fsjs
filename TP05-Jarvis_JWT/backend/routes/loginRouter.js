const express = require('express')
const router = express.Router()

const loginController = require('../controllers/loginController')
router.get('/',loginController.home)
router.post('/',loginController.homePOST)

module.exports = router